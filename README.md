# README #

The 3rd year personal project of Luke Eric Roy Davison (eeu698).

### Topic ###
Data Physicalisation Toolkit

### To-do ###
Modular interpretting of external lua files for methods of physicalisation.

Generation of STL files for 3d representation of data using 3d printers and other fabrication machines.

Generation of shape nets for 3d representation of data using standard printers and laser cutter.

### Technical Details ###
Main Language: C++

Meta Build System: Meson

Build System: Ninja
