#include <iostream>
#include <stdio.h>

//Link standard C lua headers
extern "C" {
  #include "lua.h"
  #include "lauxlib.h"
  #include "lualib.h"
}

using namespace std;

int main(int argc, char** argv){
  //Create new lua state
  lua_State * lua = luaL_newstate();

  //Open all libraries
  luaL_openlibs(lua);

  luaL_dostring(lua, "print('Hello World!')");
  //Close lua state
  lua_close(lua);

  return 0;
}
